import React, { useState, useEffect } from 'react';
import { useSelector } from "react-redux";
import Button from '../../Button';
import Card from '../../Card';
import SortPopup from '../../SortPopup';
import Filter from '../../Filter';
import './Categories.css';

function Categories({ title, category }) {

  const products = useSelector(state => state.products);

  const [prod, setProd] = useState(products);
  const [selectedSort, setSelectedSort] = useState('');
  const [min, setMin] = useState('');
  const [max, setMax] = useState('');

  useEffect(() => {
    let localState = products;
    if (selectedSort === 'lowest') {
      localState = localState.sort((a, b) => a.price - b.price);
    }
    if (selectedSort === 'highest') {
      localState = localState.sort((a, b) => b.price - a.price);
    }

    if (min !== 0) {
      localState = localState.filter(item => item.price >= min);
    }

    if (max !== '') {
      localState = localState.filter(item => item.price <= max);
    }
    setProd(localState);

  }, [selectedSort, min, max])

  const addContentToCard = prod.filter(item => item.type === category).map(itemFilter => (
    <Card
      key={itemFilter.id}
      id={itemFilter.id}
      title={itemFilter.name}
      price={itemFilter.price}
      img={itemFilter.imgUrl}
    />
  ));

  return (
    <div className="categories">
      <div className="cat-w">
        <h1>{title}</h1>
        <Filter minValue={min} maxValue={max} setValueMin={setMin} setValueMax={setMax} />
        <SortPopup setSort={setSelectedSort} />
      </div>
      <div className="wrap">
        {addContentToCard}
      </div>
      <Button title="show more" />
    </div>
  );
}

export default Categories;
