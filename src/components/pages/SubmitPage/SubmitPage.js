import React from 'react'
import useForm from '../../../hooks/useForm'
import './SubmitPage.css'

function SubmitPage() {


  const { handleSubmit, handleChangeFormField, data, errors, } = useForm({
    validations: {
      name: {

        pattern: {
          value: /^[A-Za-z]*$/,
          message:
            "Enter your name without special symbols and numbers please",
        },
        required: {
          value: true,
          message: 'This field is required',
        },
      },

      email: {
        pattern: {
          value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
          message:
            "Email address is not correct",
        },
      },

      phone: {
        pattern: {
          value: /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/,
          message:
            "Phone number is not correct",
        },
      },

      message: {
        required: {
          value: true,
          message: 'This field is required',
        },
      },

      sel: {
        required: {
          value: true,
          message: 'This field is required',
        },
      },
    },
    onSubmit: () => alert('Form submitted!'),
  });

  return (
    <div className="submit">
      <div>
        <h1 className="submit__title">Submit a ticket</h1>
        <p className="submit__text">Tickets will be responsed to within 24 hours </p>
        <p className="submit__info">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium quidem illum veritatis eaque sint nam consequatur quas enim.</p>
      </div>
      <form onSubmit={handleSubmit}>
        <div className="form-w">
          <div className="form-inner">
            <div className="form-group">
              <label className="label" htmlFor="name" >Full name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                placeholder="Name"
                value={data.name || ''}
                onChange={handleChangeFormField('name')}
              />
              {errors.name && <p className="error">{errors.name}</p>}
            </div>
            <div className="form-group">
              <label className="label" htmlFor="email">Email</label>
              <input
                type="text"
                className="form-control"
                id="email"
                placeholder="Email"
                value={data.email || ''}
                onChange={handleChangeFormField('email')}
              />
              {errors.email && <p className="error">{errors.email}</p>}
            </div>
            <div className="form-group">
              <label className="label" htmlFor="Mobile">Mobile number</label>
              <input
                type="text"
                className="form-control"
                id="Mobile"
                placeholder="Your phone"
                value={data.phone || ''}
                onChange={handleChangeFormField('phone')}
              />
              {errors.phone && <p className="error">{errors.phone}</p>}
            </div>
            <div className="form-group">
              <label className="label" >Priority</label>
              <select
                id='sel'
                className="form-control custom-select"
                value={data.sel || ''}
                onChange={handleChangeFormField('sel')}>
                <option selected>Select how urgent it is</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </select>
              {errors.sel && <p className="error">{errors.sel}</p>}
            </div>
          </div>
          <div className="form-message form-group">
            <label className="label" htmlFor="message">Your message</label>
            <textarea
              className="area form-control"
              placeholder="Enter your message"
              id="message"
              value={data.message || ''}
              onChange={handleChangeFormField('message')}>
            </textarea>
            {errors.message && <p className="error">{errors.message}</p>}

          </div>
        </div>
        <button
          type="submit" className="btn btn-primary"
          >
            Submit request
          </button>
      </form>
    </div>
  )
}

export default SubmitPage
