import React, { useState, useEffect } from 'react';
import { useSelector } from "react-redux";
import Product from '../../Product';
import Button from '../../Button';
import './CartPage.css'

function CartPage() {

  const items = useSelector(state => state.cart);
  const [totalPrice, setTotalPrice] = useState(0);

  useEffect(() => {
    let price = 0;
    items.forEach((element) => {
      price +=  element.qty * element.price
    });
    setTotalPrice(price);
  }, [items, totalPrice, setTotalPrice]);

  const addProductToCart = items.map(element =>
    <Product
      value={element.qty}
      id={element.id}
      desc={element.name}
      price={element.price}
      img={element.imgUrl}

    />
  )

  return (
    <div className="cart-page">
      <h1>Cart</h1>
      <div className="test">
        <span className="cart__header cart__header-product">Product</span>
        <span className="cart__header cart__header-price">Price</span>
        <span className="cart__header cart__header-quantity">Quantity</span>
      </div>
      <div className="products">
        {addProductToCart}
      </div>
      <div className="total">
        Total: {totalPrice} $
      </div>
      <Button title="check out" cls />
    </div>
  );
}

export default CartPage;
