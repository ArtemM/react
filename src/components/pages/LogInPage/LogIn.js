import { createUserWithEmailAndPassword, onAuthStateChanged, signInWithEmailAndPassword, signOut } from 'firebase/auth';
import { setDoc, doc, getDoc } from 'firebase/firestore';
import React, { useState, useEffect } from 'react';
import Edit from '../../Edit';
import { auth } from '../../../index';
import { db } from '../../../index';
import './LogIn.css';

function LogIn() {

  const [loginEmail, setLoginEmail] = useState('');
  const [loginPassword, setLoginPassword] = useState('');
  const [user, setUser] = useState({});

  useEffect(() => {
    onAuthStateChanged(auth, currentUser => {
      if (currentUser) {
        setUser(currentUser);
      } else {
        setUser(null);
      }
    })
  }, [user])

  const signup = async () => {
    try {
      const result = await createUserWithEmailAndPassword(
        auth,
        loginEmail,
        loginPassword
      );
      setDoc(doc(db, 'users', result.user.uid), { email: loginEmail, id: result.user.uid });
      setLoginEmail('');
      setLoginPassword('');
    } catch (error) {
      console.log(error.message);
    }
  }

  const login = async () => {
    try {
      const resultSigIn = await signInWithEmailAndPassword(
        auth,
        loginEmail,
        loginPassword
      );

      const docRef = doc(db, 'users', resultSigIn.user.uid);
      const docSnap = await getDoc(docRef);

      if (!docSnap.exists()) {
        setDoc(doc(db, 'users', resultSigIn.user.uid), { email: loginEmail, id: resultSigIn.user.uid });
      }
      setLoginEmail('');
      setLoginPassword('');
    } catch (error) {
      console.log(error.message);
    }
  }

  const logout = async () => {
    await signOut(auth);
  }

  return (
    <div>
      <form >
        <div className="wrapper">
          <div className="form-inner">
            <div className="form-group">
              <label className="label" htmlFor="login" >Your LogIn</label>
              <input
                type="text"
                className="form-control"
                id="login"
                placeholder="account"
                value={loginEmail}
                onChange={(e) => setLoginEmail(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label className="label" htmlFor="password" >Password</label>
              <input
                type="text"
                className="form-control"
                id="name"
                placeholder="Password"
                value={loginPassword}
                onChange={(e) => setLoginPassword(e.target.value)}
              />
            </div>
          </div>
          {!user ? null : <Edit test={user} />}
        </div>
        <h4>User Logged In: {user ? user?.email : ''}</h4>
        <div className="wrapper">
          <button type="button" className="btn-primary" onClick={signup}>Sign Up</button>
          {!user
            ? <button type="button" className="btn-primary" onClick={login}>Log In</button>
            : <button type="button" className="btn-primary" onClick={logout}>Log Out</button>
          }
        </div>
      </form>
    </div>
  )
}

export default LogIn;
