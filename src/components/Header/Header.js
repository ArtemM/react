import React from 'react'
import { NavLink, Link } from 'react-router-dom'
import Cart from '../Cart'
import './Header.css'

const Header = function () {

  return (
    <div className="header">
      <div className="wrapper">
        <div className="logo">SHOP LOGO</div>
        <nav className="nav">
          <ul className="nav__list">
            <li className="nav__list-item"><NavLink to='/bags'  activeClassName="active">bags</NavLink>  </li>
            <li className="nav__list-item"><NavLink to='/shoes'>shoes</NavLink></li>
            <li className="nav__list-item"><NavLink to='/hats'>hats</NavLink></li>
          </ul>
        </nav>
      </div>
      <Link to='/submitpage'>submit</Link>
      <Link to='/login'>Log In</Link>
      <Link to='/cartpage'>
        <Cart />
      </Link>
    </div>
  )
}

export default Header;