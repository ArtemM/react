import React, {useState} from 'react'
import { useDispatch } from 'react-redux';
import { removeCart, adjustQty } from '../../redux/action';
import './Product.css'

function Product({ desc, value, after, price, img, id }) {

  const dispatch = useDispatch();
  const [input, setInput] = useState(value)

  const onChangeHandler = (e) => {
    setInput(e.target.value);
    dispatch(adjustQty(id, e.target.value));
  }

  return (
    <div className="product">
      <img className="product__img" src={img} alt="bags" />
      <p className="product__descr">{desc}</p>
      <p className={`product__price ${after ? 'product__price--m' : ''}`}>{price} $</p>
      <input
        className="product__quantity"
        min="1"
        type="number"
        value={input}
        onChange={onChangeHandler}
      />
      <button
        onClick={() => {
          dispatch(removeCart({ id }));
        }}
        className="product__btn">X</button>
    </div>
  )
}

export default Product;
