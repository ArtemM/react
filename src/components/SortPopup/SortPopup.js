import React from 'react'

function SortPopup({ setSort }) {
  return (
    <div>
      <select
       onChange= {(e)=>setSort(e.target.value)}>
        <option disabled selected>Sort by</option>
        <option value="lowest">Lowest</option>
        <option value="highest">Highest</option>
      </select>
    </div>
  )
}

export default SortPopup;
