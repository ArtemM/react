import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import Categories from '../pages/Categories';
import CartPage from '../pages/CartPage';
import SubmitPage from '../pages/SubmitPage/SubmitPage';
import LogInPage from '../pages/LogInPage/LogIn';

import './Content.css';

function Content() {
  return (
    <div className="content">
      <Switch>
        <Redirect exact from='/' to='/bags' />
        <Route path='/bags'><Categories title="Bags" category="bag" /></Route>
        <Route path='/shoes'><Categories title="Shoes" category="shoes" /></Route>
        <Route path='/hats' ><Categories title="Hats" category="hat" /></Route>
        <Route path='/submitpage' component={SubmitPage} />
        <Route path='/login' component={LogInPage} />
        <Route path='/cartpage' component={CartPage} />
      </Switch>
    </div>
  );
}

export default Content;
