import React from 'react';
import { useDispatch}  from 'react-redux';
import { addCart } from '../../redux/action';
import './Card.css';

function Card({ img, title, price, id }) {

  const dispatch = useDispatch();

  return (
    <div
      onClick={()=> {
        dispatch(addCart({id}));
      }}
      className="card"
    >
      <img src={img} alt="bags" />
      <p className="descr">{title}</p>
      <p className="price">{price}$</p>
    </div>
  )
}

export default Card;
