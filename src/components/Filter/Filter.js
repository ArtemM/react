import React from 'react'
import './Filter.css'

function Filter({ minValue, setValueMin, maxValue, setValueMax }) {

  return (
    <div>
      from:
      <input
        value={minValue}
        onChange={(e) => setValueMin(e.target.value)}
        className="filter"
        type="number" />
      to:
      <input
         value={maxValue}
         onChange={(e) => setValueMax(e.target.value)}
        className="filter"
        type="number" />
    </div>
  )
}

export default Filter
