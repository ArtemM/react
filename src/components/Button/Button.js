import React from 'react';
import './Button.css'

function Button({title, cls}) {

  return (
    <div className={`button ${cls ? 'button--cart' : ''}`}>
      <button className="btn-show">{title}</button>
    </div>
  );
}

export default Button;