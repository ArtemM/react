import React, { useState, useEffect } from 'react'
import logo from '../../Cart.svg'
import './Cart.css'
import { useSelector } from "react-redux";

function Cart() {

  const items = useSelector(state => state.cart);
  const [cartCount, setCartCount] = useState(0);
  const [price, setPrice] = useState(0);

  useEffect(() => {
    let count = 0;
    let price = 0;
    items.forEach((element) => {
      count += element.qty;
      price += element.qty * element.price
    });

    setPrice(price);
    setCartCount(count);
  },[items,cartCount]);

  return (
    <div className="cart">
      {cartCount} items /&nbsp;<span className="cart__price">{price}$</span>
      <img src={logo} alt="" />
    </div>
  )
}

export default Cart;