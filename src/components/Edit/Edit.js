import React, { useState, useEffect } from 'react'
import { doc, getDoc, updateDoc } from 'firebase/firestore';
import { db } from '../../index';
import './Edit.css'

function Edit({ test }) {

  const [userInfo, setUserInfo] = useState({
    age: '',
    email: '',
    fullName: '',
    id: ''
  });

  useEffect(async () => {
    try {
      const docRef = doc(db, 'users', test.uid);
      const docSnap = await getDoc(docRef);

      if (docSnap.exists()) {
        const user = docSnap.data();
        setUserInfo({
          age: user?.age ? user.age : '',
          email: user?.email ? user.email : '',
          fullName: user?.fullName ? user.fullName : '',
          id: docSnap.id
        });
      }
    } catch (error) {
      console.log(error.message);
    }

  }, [])

  const onChange = (e) => {
    const state = userInfo;
    setUserInfo({...state, [e.target.name] : e.target.value
    });
  }

  const upd = async () => {
    const { fullName, age, email, id } = userInfo;
    updateDoc(doc(db, 'users', test.uid), {
      age: age,
      email: email,
      fullName: fullName ,
      id: id
    })
  }

  return (
    <div className="edit">
      <div className="wrap">
        <div className="form-group">
          <label className="label" htmlFor="name" >Your Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="fullName"
            placeholder="Name"
            value={userInfo.fullName}
            onChange={onChange}
          />
        </div>
        <div className="form-group">
          <label className="label" htmlFor="age" >Your Age</label>
          <input
            type="text"
            className="form-control"
            id="age"
            name="age"
            placeholder="Age"
            value={userInfo.age}
          onChange={onChange}
          />
        </div>
        <div className="form-group">
          <label className="label" htmlFor="email" >Your Email</label>
          <input
            type="text"
            className="form-control"
            id="email"
            name="email"
            placeholder="Email"
            value={userInfo.email}
          onChange={onChange}
          />
        </div>
        <div className="form-group">
          <label className="label" htmlFor="password" >Your Password</label>
          <input
            type="text"
            className="form-control"
            id="password"
            placeholder="Password"
          // value={loginEmail}
          // onChange={}
          />
        </div>
      </div>

      <button type="button" className="btn-primary" onClick={upd}>Update</button>
    </div>
  )
}

export default Edit
