import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './redux/store';
import { HashRouter } from 'react-router-dom';
import { initializeApp } from "firebase/app";
import 'firebase/firestore';
import 'firebase/auth';
import App from './App';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {

  apiKey: "AIzaSyBaflvG5UxNpiYRQ1iXmQnZVp1zS1amzDU",

  authDomain: "login-test-67301.firebaseapp.com",

  projectId: "login-test-67301",

  storageBucket: "login-test-67301.appspot.com",

  messagingSenderId: "713037494653",

  appId: "1:713037494653:web:801a039092d4c858eb7414",

  measurementId: "G-CCZ44DDW74"

};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth();
export const db = getFirestore(app);

ReactDOM.render(

  <Provider store={store}>
    <HashRouter>
      <App />
    </HashRouter>
  </Provider>,
  document.getElementById('root')
);

