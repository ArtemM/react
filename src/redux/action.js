import * as actions from './actionType';

export const addCart = (itemID) => ({
  type: actions.ADD_TO_CART,
  payload: itemID
});

export const removeCart = (itemID) => ({
  type: actions.REMOVE_FROM_CART,
  payload: itemID
});

export const adjustQty = (itemID, value) => ({
  type: actions.ADJUST_QTY,
  payload: {
    id: itemID,
    qty: value
  }
});

export const sortBy = (sortType) => ({
  type: actions.SORT_BY_PRICE,
  payload: sortType
});

export const filter = (filterType, filterValue) => ({
  type: actions.FILTER_BY_PRICE,
  payload: {
    type: filterType,
    value: filterValue
  }
});
