export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_TO_CART';
export const ADJUST_QTY = 'ADJUST_QTY';
export const SORT_BY_PRICE = 'SORT_BY_PRICE';
export const FILTER_BY_PRICE = 'FILTER_BY_PRICE';


