import * as actions from '../actionType';

const initialState = {
  cart: [],
  test: [],
  products: [
    { id: 0, name: 'Item', price: 90, imgUrl: '/img/hats-1.png', type: 'hat' },
    { id: 1, name: 'Item', price: 50, imgUrl: '/img/hats-1.png', type: 'hat' },
    { id: 2, name: 'Item', price: 300, imgUrl: '/img/hats-1.png', type: 'hat' },
    { id: 3, name: 'Item', price: 900, imgUrl: '/img/hats-1.png', type: 'hat' },
    { id: 4, name: 'Item', price: 100, imgUrl: '/img/hats-1.png', type: 'hat' },
    { id: 5, name: 'Item', price: 200, imgUrl: '/img/hats-1.png', type: 'hat' },
    { id: 6, name: 'Item', price: 300, imgUrl: '/img/hats-1.png', type: 'hat' },
    { id: 7, name: 'Item', price: 400, imgUrl: '/img/hats-1.png', type: 'hat' },
    { id: 8, name: 'Item', price: 300, imgUrl: '/img/hats-1.png', type: 'hat' },
    { id: 9, name: 'Item', price: 400, imgUrl: '/img/hats-1.png', type: 'hat' },
    { id: 10, name: 'Item', price: 100, imgUrl: '/img/bag-1.png', type: 'bag' },
    { id: 11, name: 'Item', price: 500, imgUrl: '/img/bag-1.png', type: 'bag' },
    { id: 12, name: 'Item', price: 300, imgUrl: '/img/bag-1.png', type: 'bag' },
    { id: 13, name: 'Item', price: 900, imgUrl: '/img/bag-1.png', type: 'bag' },
    { id: 14, name: 'Item', price: 100, imgUrl: '/img/bag-1.png', type: 'bag' },
    { id: 15, name: 'Item', price: 200, imgUrl: '/img/bag-1.png', type: 'bag' },
    { id: 16, name: 'Item', price: 300, imgUrl: '/img/bag-1.png', type: 'bag' },
    { id: 17, name: 'Item', price: 400, imgUrl: '/img/bag-1.png', type: 'bag' },
    { id: 18, name: 'Item', price: 300, imgUrl: '/img/bag-1.png', type: 'bag' },
    { id: 19, name: 'Item', price: 400, imgUrl: '/img/bag-1.png', type: 'bag' },
    { id: 20, name: 'Item', price: 100, imgUrl: '/img/bag-1.png', type: 'shoes' },
    { id: 21, name: 'Item', price: 400, imgUrl: '/img/bag-1.png', type: 'shoes' },
    { id: 22, name: 'Item', price: 500, imgUrl: '/img/bag-1.png', type: 'shoes' },
    { id: 23, name: 'Item', price: 600, imgUrl: '/img/bag-1.png', type: 'shoes' },
    { id: 24, name: 'Item', price: 100, imgUrl: '/img/bag-1.png', type: 'shoes' },
    { id: 25, name: 'Item', price: 300, imgUrl: '/img/bag-1.png', type: 'shoes' },
    { id: 26, name: 'Item', price: 300, imgUrl: '/img/bag-1.png', type: 'shoes' },
    { id: 27, name: 'Item', price: 400, imgUrl: '/img/bag-1.png', type: 'shoes' },
    { id: 28, name: 'Item', price: 5000, imgUrl: '/img/bag-1.png', type: 'shoes' },
    { id: 29, name: 'Item', price: 400, imgUrl: '/img/bag-1.png', type: 'shoes' },
  ]
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case actions.ADD_TO_CART:

      const prod = state.products.find(prod => prod.id === action.payload.id);
      const inCart = state.cart.find(item => item.id === action.payload.id);

      return {
        ...state,
        cart: inCart
          ? state.cart.map(item => item.id === action.payload.id
            ? { ...item, qty: item.qty + 1 }
            : item)
          : [...state.cart, { ...prod, qty: 1 }]
      }

    case actions.REMOVE_FROM_CART:
      return {
        ...state,
        cart: state.cart.filter(item => item.id !== action.payload.id),
      }

    case actions.ADJUST_QTY:
      return {
        ...state,
        cart: state.cart.map(item => item.id === action.payload.id
          ? { ...item, qty: +action.payload.qty }
          : item
        ),
      }

    // case actions.SORT_BY_PRICE:

    //   const sortProducts = state.products.slice();
    //   console.log(action.payload);

    //   return {
    //     ...state,
    //     products: action.payload === 'lowest' ? sortProducts.sort((a, b) => a.price - b.price) : sortProducts.sort((a, b) => b.price - a.price)
    //   }

    // case actions.FILTER_BY_PRICE:

    //   const filterProducts = state.products.slice();

    //   console.log(filterProducts);

    //   return {
    //     ...state,
    //     products: action.payload.type === 'from' && action.payload.value !== '' ? filterProducts.filter((a) => a.price >= action.payload.value) : filterProducts
    //   }

    default:
      return state;
  }
}

export default reducer;